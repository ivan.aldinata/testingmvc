//
//  HomeView.swift
//  TestingMVC
//
//  Created by Ivan on 15/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

class HomeView: UIViewController {
    var controller: HomeViewToController?
    
    @IBOutlet weak var nameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        controller?.viewDidLoad()
    }
    
    public func setupView() {
        view.backgroundColor = .red
    }
    public func setName(name: String) {
        nameLabel.text = name
    }
    @IBAction func moveToSecondView(_ sender: Any) {
        controller?.fromHomeToMenu()
    }
}
extension HomeView: HomeControllerToView {
    func navigateToSecondView(with vc: UIViewController) {
        self.present(vc, animated: true, completion: nil)
    
    }
}
