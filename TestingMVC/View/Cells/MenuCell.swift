//
//  MenuCell.swift
//  TestingMVC
//
//  Created by Ivan on 26/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleImage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setupCell(data: Menus) {
        mainImage.image = UIImage(named: data.image)
        titleImage.text = data.title
    }

}
