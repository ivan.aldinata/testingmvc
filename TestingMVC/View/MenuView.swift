//
//  MenuView.swift
//  TestingMVC
//
//  Created by Ivan on 17/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

class MenuView: UIViewController {
    var controller: MenuViewToController?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controller?.viewDidLoad()
    }
}
extension MenuView: MenuControllerToView {
    func setupView() {
        self.view.backgroundColor = .white
        titleLabel.text = "Hello"
    }
}

extension MenuView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return controller?.getNumberofSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuCell
        cell.setupCell(data: controller?.getCellData(for: indexPath) ?? Menus(image: "", title: "") )
        
        return cell
    }
    
    
}
