//
//  MenuProtocol.swift
//  TestingMVC
//
//  Created by Ivan on 17/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

protocol MenuControllerToView: class {
    var controller: MenuViewToController? {get set}
    func setupView()
}

protocol MenuControllerToModel: class {
    var controller: MenuModelToController? {get set}
    func getSection() -> Int
    func getRows() -> Int
    func getData(index: IndexPath) -> Menus
}

protocol MenuViewToController: class {
    var view: MenuControllerToView? {get set}
    var model: MenuControllerToModel? {get set}
    func viewDidLoad()
    func getNumberofSection() -> Int
    func getNumberOfRows() -> Int
    func getCellData(for index: IndexPath) -> Menus
}

protocol MenuModelToController: class {
    
}
