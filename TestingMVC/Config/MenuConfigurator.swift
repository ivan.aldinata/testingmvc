//
//  MenuConfigurator.swift
//  TestingMVC
//
//  Created by Ivan on 17/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

public class MenuConfigurator {
    public static var shared = MenuConfigurator()
    
    public func createMenuView() -> UIViewController {
        let controller: MenuViewToController & MenuModelToController = MenuController()
        let view: UIViewController & MenuControllerToView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MenuView") as! MenuView
        let model: MenuControllerToModel = MenuModel()
        
        view.controller = controller
        controller.view = view
        controller.model = model
        model.controller = controller
        return view
    }
}
