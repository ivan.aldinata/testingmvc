//
//  HomeConfigurator.swift
//  TestingMVC
//
//  Created by Ivan on 15/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

public class HomeConfigurator {
    public static var shared = HomeConfigurator()
    
    public func createHomeView() -> UIViewController {
        let controller: HomeViewToController & HomeModelToController = HomeController()
        let view: UIViewController & HomeControllerToView = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! HomeView
        let model: HomeControllerToModel = HomeModel()
        
        view.controller = controller
        controller.view = view
        controller.model = model
        model.controller = controller
        return view
    }
}
