//
//  HomeProtocol.swift
//  TestingMVC
//
//  Created by Ivan on 15/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

protocol HomeControllerToView: class {
    var controller: HomeViewToController? {get set}
    
    func setupView()
    func setName(name: String)
    func navigateToSecondView(with vc: UIViewController)
}
protocol HomeControllerToModel: class {
    var controller: HomeModelToController? {get set}
    
    func getName()
}
protocol HomeViewToController: class {
    var view: HomeControllerToView? {get set}
    var model: HomeControllerToModel? {get set}
    
    func viewDidLoad()
    func fromHomeToMenu()
}
protocol HomeModelToController: class {
    func didGetName(with name: String)
}
