//
//  MenuController.swift
//  TestingMVC
//
//  Created by Ivan on 17/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

public class MenuController: MenuViewToController {

    var view: MenuControllerToView?
    var model: MenuControllerToModel?
    
    func viewDidLoad() {
        view?.setupView()
    }
    func getNumberofSection() -> Int {
        return model?.getSection() ?? 0
    }
    
    func getNumberOfRows() -> Int {
        return model?.getRows() ?? 0
    }
    
    func getCellData(for index: IndexPath) -> Menus {
        return model?.getData(index: index) ?? Menus(image: "", title: "")
    }
}
extension MenuController: MenuModelToController {
    
}
