//
//  ViewController.swift
//  TestingMVC
//
//  Created by Ivan on 15/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import UIKit

public class HomeController: HomeViewToController {
    
    var view: HomeControllerToView?
    var model: HomeControllerToModel?
    
    func viewDidLoad() {
        view?.setupView()
        model?.getName()
    }
}
extension HomeController: HomeModelToController {
    func didGetName(with name: String) {
        view?.setName(name: name)
    }
    func fromHomeToMenu() {
        let menuConfig = MenuConfigurator()
        let vc = MenuConfigurator.createMenuView(menuConfig)
        view?.navigateToSecondView(with: vc())
    }
}
