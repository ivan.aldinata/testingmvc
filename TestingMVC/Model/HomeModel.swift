//
//  HomeModel.swift
//  TestingMVC
//
//  Created by Ivan on 15/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import Foundation

public class HomeModel: HomeControllerToModel {
   
    var controller: HomeModelToController?
    
    func getName() {
        controller?.didGetName(with: "Kevin")
    }
}
