//
//  MenuModel.swift
//  TestingMVC
//
//  Created by Ivan on 17/07/20.
//  Copyright © 2020 Ivan. All rights reserved.
//

import Foundation
public struct Menus {
    var image: String
    var title: String
    
    public init(image: String, title: String) {
        self.image = image
        self.title = title
    }
}
public class MenuModel: MenuControllerToModel {
    
    var controller: MenuModelToController?
    var Menu =  [Menus(image: "Test1", title: "Ayambos"), Menus(image: "Test2", title: "SapiBos")]
    
    func getSection() -> Int {
        return 1
    }
    
    func getRows() -> Int {
        return 2
    }
    
    func getData(index: IndexPath) -> Menus {
        return Menu[index.row]
    }
    
    //kalo ambil data dari API baru di return pakek controller itu, nanti ada functionnya didGetData dll.
}
